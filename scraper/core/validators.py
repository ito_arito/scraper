from schematics.models import Model
from schematics.types import URLType, StringType, DateTimeType, IntType


class TagValidator(Model):
    tag = StringType(required=True)
    image = StringType(default='')


class VideoTagValidator(Model):
    tag_id = IntType(required=True)
    video_id = IntType(required=True)


class VideoValidator(Model):
    title = StringType(required=True)
    url = URLType(required=True)
    key = StringType(required=True)
    img = StringType(required=True)
    thumb = StringType(default='')
    # duration = DateTimeType(required=True, formats=('%H:%M:%S', '%M:%S'))
    duration = StringType(required=True)
    views = StringType(required=True)
    embed = StringType(required=True)
    rate = StringType(default='')
    site_id = IntType(required=True)
