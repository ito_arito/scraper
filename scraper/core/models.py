from peewee import *

database = MySQLDatabase('shicorotto', **{'charset': 'utf8', 'use_unicode': True, 'host': '127.0.0.1', 'port': 13306, 'user': 'root', 'password': 'root'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class Users(BaseModel):
    created_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    email = CharField()
    group = IntegerField(constraints=[SQL("DEFAULT 1")])
    last_login = CharField()
    login_hash = CharField()
    password = CharField()
    profile_fields = TextField()
    updated_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    username = CharField()

    class Meta:
        table_name = 'users'

class Sites(BaseModel):
    created_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    site = CharField(unique=True)
    updated_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    class Meta:
        table_name = 'sites'

class Videos(BaseModel):
    created_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    duration = CharField(constraints=[SQL("DEFAULT ''")])
    embed = CharField(constraints=[SQL("DEFAULT ''")])
    img = CharField(constraints=[SQL("DEFAULT ''")])
    key = CharField(unique=True)
    rate = CharField(constraints=[SQL("DEFAULT 0.0")])
    site = ForeignKeyField(column_name='site_id', field='id', model=Sites)
    summary = CharField(constraints=[SQL("DEFAULT ''")])
    thumb = CharField(constraints=[SQL("DEFAULT ''")])
    title = CharField(constraints=[SQL("DEFAULT ''")])
    updated_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    url = CharField(constraints=[SQL("DEFAULT ''")])
    views = CharField(constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'videos'

class Likes(BaseModel):
    created_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    updated_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    user = ForeignKeyField(column_name='user_id', field='id', model=Users)
    video = ForeignKeyField(column_name='video_id', field='id', model=Videos)

    class Meta:
        table_name = 'likes'

class Migration(BaseModel):
    migration = CharField(constraints=[SQL("DEFAULT ''")])
    name = CharField()
    type = CharField()

    class Meta:
        table_name = 'migration'
        primary_key = False

class Tags(BaseModel):
    created_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    tag = CharField(unique=True)
    updated_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    class Meta:
        table_name = 'tags'

class Unlikes(BaseModel):
    created_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    updated_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    user = ForeignKeyField(column_name='user_id', field='id', model=Users)
    video = ForeignKeyField(column_name='video_id', field='id', model=Videos)

    class Meta:
        table_name = 'unlikes'

class VideosTags(BaseModel):
    created_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    tag = ForeignKeyField(column_name='tag_id', field='id', model=Tags)
    updated_at = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    video = ForeignKeyField(column_name='video_id', field='id', model=Videos)

    class Meta:
        table_name = 'videos_tags'

