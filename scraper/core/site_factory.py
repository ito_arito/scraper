from abc import ABCMeta, abstractmethod


class SiteFactory(metaclass=ABCMeta):
    def __init__(self, target, detail_only=False):
        self.target = target
        self.detail_only = detail_only

    @abstractmethod
    def get_main_selector(self):
        pass

    @abstractmethod
    def get_list_extractor(self):
        pass

    @abstractmethod
    def get_detail_extractor(self):
        pass
