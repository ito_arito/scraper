#!/usr/bin/env bash

HOST="127.0.0.1"
PORT="13306"
USER="root"
PASSWORD="root"
DATABASE="shicorotto"
TEST_DATABASE="shicorotto_test"

python -m pwiz -e mysql -H ${HOST} -p ${PORT} -u ${USER} -P ${PASSWORD} ${DATABASE} > models.py