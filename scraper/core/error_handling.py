
import requests
from retrying import retry


# status code described temporary error
TEMPORARY_ERROR_CODES = (408, 500, 502, 503, 504)


@retry(stop_max_attempt_number=3, wait_exponential_multiplier=1000)
def test_fetch(url: str):

    print('Retrieving {0}...'.format(url))
    response = requests.get(url)
    print('Status: {0}'.format(response.status_code))

    if response.status_code in TEMPORARY_ERROR_CODES:
        raise Exception(f'Temporary Error: {response.status_code}')
    elif not (200 <= response.status_code < 300):
        raise Exception(f'Exception occured: {response.status_code}')