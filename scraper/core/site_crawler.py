import time
import requests
import lxml.html
from lxml import etree


from schematics.exceptions import DataError, ValidationError
from core.error_handling import test_fetch
from core.builder import ExtractorDict
from core.base_site import BaseSite
from core.utils import BaseUtil


class SiteCrawler:
    def __init__(self, site: BaseSite, util: BaseUtil):
        self.site = site
        self.util = util

    def scrape_listitem(self, response: object, key, ext):
        listitem = self.scrape_list_page(response, ext['selector'], {'listitem': ext['listitem']})
        listitem = [item['listitem'] for item in listitem]
        return {key: '' if len(listitem) == 0 else ','.join(listitem)}

    def scrape_detail_page(self, response: object, extractor: dict):
        root = lxml.html.fromstring(response.content)
        ext_dict = ExtractorDict(root)
        ret = {}
        for key, ext in extractor.items():
            if 'listitem' in ext:
                ret.update(self.scrape_listitem(response, key, ext))
                continue

            ext_dict[key] = ext

        ret.update(ext_dict.content)
        return ret

    def scrape_list_page(self, response: object, selector: str, extractor: dict):
        root = lxml.html.fromstring(response.content)
        root.make_links_absolute(response.url)

        for video in root.cssselect(selector):

            ext_dict = ExtractorDict(video)
            for key, ext in extractor.items():
                ext_dict[key] = ext
            yield ext_dict.content

    def scrape_one_page(self, session: object, uri: str, delay: int = 1):
        test_fetch(uri)
        response = session.get(uri)

        for item in self.scrape_list_page(response, self.site.main_selector, self.site.list_extractor):

            time.sleep(delay)
            response = session.get(item['url'])

            detail_page = self.scrape_detail_page(response, self.site.detail_extractor)

            item.update(detail_page)

            self.util.saver(item)

    def run_only_details(self, urls: str, delay: int):
        session = requests.Session()

        for url in urls:
            test_fetch(url)

            time.sleep(1)
            response = session.get(url)

            item = self.scrape_detail_page(response, self.site.detail_extractor)
            item['url'] = url

            self.util.saver(item)

    def run(self, start: int, end: int, delay: int = 1):
        session = requests.Session()

        for i in range(start, end):
            uri = self.util.BASE_URL if i == 1 else self.util.get_page(i)
            self.scrape_one_page(session, uri, delay)


