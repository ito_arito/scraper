
import time
from core.site_crawler import SiteCrawler
from core.base_site import BaseSite
from redtube.factory import RedtubeFactory

def read_urls_file(filename):
    with open(filename) as f:
        while True:
            line = f.readline()
            if not line: break
            yield line.rstrip('\r\n')

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Crawler')
    subparser = parser.add_subparsers(help='sub-command help')

    parser.add_argument('--site', '-S', default='redtube', type=str, help='Crawler site')
    parser.add_argument('--target', '-t', default='video', type=str, help='Crawler target elements')
    parser.add_argument('--delay', '-d', type=int, default=1, help='Delay')
    parser.add_argument('--start', '-s', type=int, default=1, help='Start')
    parser.add_argument('--end', '-e', type=int, default=2, help='End')
    parser.set_defaults(only_details=False)

    from_file = subparser.add_parser('from-file')
    from_file.add_argument('--list-file', '-f', default='./redtube/urls.txt', type=str, help='Scrape detail page list')
    from_file.add_argument('--site', '-S', default='redtube', type=str, help='Crawler site')
    from_file.add_argument('--target', '-t', default='video', type=str, help='Crawler target elements')
    from_file.add_argument('--delay', '-d', type=int, default=1, help='Delay')
    from_file.set_defaults(only_details=True)

    args = parser.parse_args()

    if args.site == 'redtube':
        factory = RedtubeFactory(args.target, args.only_details)
    else:
        raise ValueError

    site = BaseSite(factory.get_main_selector())
    site.list_extractor = factory.get_list_extractor()
    site.detail_extractor = factory.get_detail_extractor()

    sc = SiteCrawler(site, factory.get_util())

    if args.only_details:
        sc.run_only_details(read_urls_file(args.list_file), args.delay)
    else:
        sc.run(args.start, args.end, args.delay)